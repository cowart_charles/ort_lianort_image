FROM canvasslabs/ortbase
RUN curl --insecure -o lianort.zip https://rivera.canvasslabs.com:5000/client/download/v1.3.1/linux
RUN unzip lianort.zip
RUN rm lianort.zip
#RUN git clone --recurse-submodules https://github.com/oss-review-toolkit/ort.git
#RUN cd ort && ./gradlew installDist --stacktrace
